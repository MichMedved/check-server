var express = require('express')
var axios = require('axios')
var app = express()
var sg = require('sendgrid')('yours api key sendgrid');

app.get('/', function(req, res){
  res.send('It works');
});

var hosts = ['https://example.com/','http://example2.com/'];


var i = 0;

let check = setInterval(function() {
    hosts.forEach(function (host) {
        axios.get(host)
        .then(function (response) {
            console.log(response.status);
        })
        .catch(function (error) {
            console.log(error.status);
            console.log(i)
            if(i === 0){send_mail('your@email.com',host); console.log('= 0 '+i)}
            if(i === 6){send_mail('your@email.com',host); i = 0; console.log('= 5 '+i)}
            
            i++;
        });
    });
  }, 1000 * 60 * 60)


function send_mail(user_mail,bad_host){
var request = sg.emptyRequest({
  method: 'POST',
  path: '/v3/mail/send',
  body: {
    personalizations: [
      {
        to: [
          {
            email: user_mail,
          },
        ],
        subject: 'Hello...This is notification from service CheckServer',
      },
    ],
    from: {
      email: 'notification@checkserver.com',
    },
    content: [
      {
        type: 'text/plain',
        value: bad_host +' stopped responding to our requests! Check if it works correctly.',
      },
    ],
  },
});

//With promise
sg.API(request)
  .then(response => {
    console.log(response.statusCode);
    console.log(response.body);
    console.log(response.headers);
  })
  .catch(error => {
    //error is an instance of SendGridError
    //The full response is attached to error.response
    console.log(error.response.statusCode);
  });
} 


app.listen(process.env.PORT || 3000)